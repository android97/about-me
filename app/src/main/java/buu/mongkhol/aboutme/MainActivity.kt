package buu.mongkhol.aboutme

import android.content.Context
import android.inputmethodservice.InputMethodService
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var myButton: Button
    lateinit var editText: EditText
    lateinit var nicknameTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myButton = findViewById(R.id.done_button)
        editText = findViewById(R.id.nickname_edit)
        nicknameTextView = findViewById(R.id.nickname_text)
        myButton.setOnClickListener{v ->
            addNickname(v)
        }

        nicknameTextView.setOnClickListener{ v ->
            updateNickname(v)
        }

    }

    private fun addNickname(v: View) {
        myButton.visibility = View.GONE
        editText.visibility = View.GONE
        nicknameTextView.text = editText.text
        nicknameTextView.visibility = View.VISIBLE

        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)

    }

    private fun updateNickname(v: View) {
        editText.visibility = View.VISIBLE
        myButton.visibility = View.VISIBLE
        v.visibility = View.GONE
        editText.requestFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(editText, 0)
    }
}